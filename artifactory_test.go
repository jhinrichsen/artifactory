package artifactory

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"testing"
	"time"
)

func TestParseSampleCliConfiguration(t *testing.T) {
	buf := []byte(`{
              "artifactory": [
                {
                  "url": "https://.../artifactory/",
                  "user": "<your user ID>",
                  "password": "<your password>",
                  "serverId": "ID7",
                  "isDefault": true
                }
              ],
              "Version": "1"
            }`)

	var cfg CliConfiguration
	if err := json.Unmarshal(buf, &cfg); err != nil {
		t.Fatal(err)
	}
	wantVersion := "1"
	gotVersion := cfg.Version
	if wantVersion != gotVersion {
		t.Fatalf("want version %q but got version %q", wantVersion, gotVersion)
	}
	if len(cfg.Artifactory) < 1 {
		t.Fatalf("want len >= 1 but got %d",
			len(cfg.Artifactory))
	}
	a := cfg.Artifactory[0]
	wantString := "ID7"
	gotString := a.ServerID
	if wantString != gotString {
		t.Fatalf("want %q but got %q", wantString, gotString)
	}

	wantBool := true
	gotBool := a.IsDefault
	if wantBool != gotBool {
		t.Fatalf("want %t but got %t", wantBool, gotBool)
	}
}

func TestLocalCliConfiguration(t *testing.T) {
	var cfg CliConfiguration
	filename, err := jfrogCliConfigFilename()
	if err != nil {
		t.Fatal(err)
	}
	buf, err := ioutil.ReadFile(filename)
	if err != nil {
		t.Fatal(err)
	}
	if err := json.Unmarshal(buf, &cfg); err != nil {
		t.Fatal(err)
	}
	wantDefaults := 1
	gotDefaults := 0
	for _, a := range cfg.Artifactory {
		if a.IsDefault {
			gotDefaults++
		}
	}
	if wantDefaults != gotDefaults {
		t.Fatalf("want %d defaults but got %d", wantDefaults, gotDefaults)
	}
}

func TestArtifactoryDefault(t *testing.T) {
	cfg, err := HomeCliConfiguration()
	if err != nil {
		t.Fatal(err)
	}
	def, err := cfg.ArtifactoryDefault()
	if err != nil {
		t.Fatal(err)
	}
	s := def.ServerID
	if len(s) == 0 {
		t.Fatalf("error: empty default Artifactory server ID")
	}
}

func TestMaybeOnlinePing(t *testing.T) {
	configured, err := jfrogCliConfigExists()
	if err != nil {
		t.Fatal(err)
	}
	if !configured {
		log.Println("skipping unconfigured test")
		return
	}
	cfg, err := HomeCliConfiguration()
	if err != nil {
		t.Fatal(err)
	}
	def, err := cfg.ArtifactoryDefault()
	if err != nil {
		t.Fatal(err)
	}
	if err := def.SystemPing(); err != nil {
		t.Fatal(err)
	}
}

func TestParseSearchResult(t *testing.T) {
	buf := []byte(`{
		"results": [
		{
		   "uri": "http://localhost:8081/artifactory/api/storage/libs-release-local/org/acme/lib/ver/lib-ver.pom"
		},{
		"uri": "http://localhost:8081/artifactory/api/storage/libs-release-local/org/acme/lib/ver2/lib-ver2.pom"
		}
		]
	}`)
	var asr ArtifactSearchResults
	if err := json.Unmarshal(buf, &asr); err != nil {
		t.Fatal(err)
	}
	want := "http://localhost:8081/artifactory/api/storage/libs-release-local/org/acme/lib/ver2/lib-ver2.pom"
	got := asr.Results[1].URI
	if want != got {
		t.Fatalf("want %q but got %q", want, got)
	}
}

func TestParseSearchResultExtended(t *testing.T) {
	asrs := sampleSearchResult(t)
	wantLen := 1
	gotLen := len(asrs.Results)
	if wantLen != gotLen {
		t.Fatalf("want len=%d but got %d", wantLen, gotLen)
	}
	asr := asrs.Results[0]

	// sample string
	wantRepo := "bintray"
	gotRepo := asr.Repo
	if wantRepo != gotRepo {
		t.Fatalf("want repo=%q but got %q", wantRepo, gotRepo)
	}

	// sample time
	layout := "2006-01-02T15:04:05.000Z"
	wantTime, err := time.Parse(layout, "2019-01-02T00:00:00.000Z")
	if err != nil {
		t.Fatal(err)
	}
	gotTime := time.Time(asr.Created)
	if wantTime != gotTime {
		t.Fatalf("want created=%v but got %v", wantTime, gotTime)
	}

	// sample embedded struct
	wantChecksum := "91d19d1709ba0ee3bb267b09eedc1f577419fa4595689c924508073e89e46849"
	gotChecksum := asr.Checksums.Sha256
	if wantChecksum != gotChecksum {
		t.Fatalf("want checksum>sha256=%q but got %q", wantChecksum, gotChecksum)
	}
}

func TestMaybeOnlineSearch(t *testing.T) {
	configured, err := jfrogCliConfigExists()
	if err != nil {
		t.Fatal(err)
	}
	if !configured {
		log.Println("skipping unconfigured test")
		return
	}
	cfg, err := HomeCliConfiguration()
	if err != nil {
		t.Fatal(err)
	}
	def, err := cfg.ArtifactoryDefault()
	if err != nil {
		t.Fatal(err)
	}
	asrs, err := def.SearchArtifact(SearchParameter{"", "jfrog.exe", true, false})
	if err != nil {
		t.Fatal(err)
	}
	log.Printf("search returns %d element(s)\n", len(asrs.Results))
	for _, asr := range asrs.Results {
		fmt.Printf("%+v\n", asr)
	}
}

func sampleSearchResult(t *testing.T) ArtifactSearchResults {
	buf := []byte(`{
  "results" : [ {
    "repo" : "bintray",
    "path" : "/jfrog/jfrog-cli-go/1.26.2/jfrog-cli-windows-amd64/jfrog.exe",
    "created" : "2019-01-02T00:00:00.000Z",
    "createdBy" : "schlackwurst",
    "lastModified" : "2019-01-02T14:19:13.000Z",
    "modifiedBy" : "schlackwurst",
    "lastUpdated" : "2019-01-02T08:12:19.538Z",
    "downloadUri" : "https://dl.bintray.com//jfrog/jfrog-cli-go/1.26.2/jfrog-cli-windows-amd64/jfrog.exe",
    "remoteUrl" : "https://dl.bintray.com//jfrog/jfrog-cli-go/1.26.2/jfrog-cli-windows-amd64/jfrog.exe",
    "mimeType" : "application/octet-stream",
    "size" : "19235328",
    "checksums" : {
      "sha1" : "aa6d2af9a047ba0474e86345eccd8abf07d4a4ce",
      "md5" : "c8b962aecba7ef560bb24a8d0055e75f",
      "sha256" : "91d19d1709ba0ee3bb267b09eedc1f577419fa4595689c924508073e89e46849"
    },
    "uri" : "https://dl.bintray.com//jfrog/jfrog-cli-go/1.26.2/jfrog-cli-windows-amd64/jfrog.exe"
  } ] }`)
	var asrs ArtifactSearchResults
	if err := json.Unmarshal(buf, &asrs); err != nil {
		t.Fatal(err)
	}
	return asrs
}

func TestBetween(t *testing.T) {
	t1 := time.Date(2019, time.January, 1, 0, 0, 0, 0, time.UTC)
	t2 := time.Date(2019, time.February, 1, 0, 0, 0, 0, time.UTC)
	asrs := sampleSearchResult(t).Between(t1, t2)
	wantLen := 1
	gotLen := len(asrs)
	if wantLen != gotLen {
		t.Fatalf("want len %d but got %d", wantLen, gotLen)
	}
}

// return default configuration from ${HOME} based JFrog CLI setup
func client(t *testing.T) Configuration {
	cfg, err := HomeCliConfiguration()
	if err != nil {
		t.Fatal(err)
	}
	def, err := cfg.ArtifactoryDefault()
	if err != nil {
		t.Fatal(err)
	}
	return *def
}

func TestContent(t *testing.T) {
	wantFilename := "testdata/log4j-1.2.17.pom"
	want, err := ioutil.ReadFile(wantFilename)
	if err != nil {
		t.Fatal(err)
	}
	downloadURI := searchContentURL(t)

	rt := client(t)
	got, err := rt.Content(downloadURI)
	if err != nil {
		t.Fatal(err)
	}

	if !bytes.Equal(want, got) {
		// store in temporary location for analysis
		gotFilename, err := save(got)
		if err != nil {
			t.Fatalf("could not save content to %q: %v",
				wantFilename, err)
		}
		t.Fatalf("content of %q does not match %q", wantFilename, gotFilename)
	}
}

const sampleArtifact = "log4j-1.2.17.pom"
const sampleChecksum = "3b95a3d3cdd3aa4b91ab327ddb5a1bfe03d81e273794e36aa1440471d5d70e5e"

// find log4j-1.2.17.pom in connected repository
// cannot return path only as the context root cannot be determined, using full
// download URL
func searchContentURL(t *testing.T) string {
	srv := client(t)
	asrs, err := srv.SearchArtifact(SearchParameter{"", sampleArtifact, true, false})
	if err != nil {
		t.Fatal(err)
	}
	log.Printf("search returns %d element(s)\n", len(asrs.Results))
	for _, asr := range asrs.Results {
		URI := asr.DownloadURI
		// Not all remote repositories support SHA256, we'll just ignore
		// those for now
		chk := asr.Checksums.Sha256
		if chk == sampleChecksum {
			fmt.Printf("using artifact %s %s\n", URI, chk)
			return URI
		}
		log.Printf("skipping prospect %s, want=%s, got=%s",
			URI, sampleChecksum, chk)
	}
	t.Fatalf("no artifact %q found that has expected checksum %q",
		sampleArtifact, sampleChecksum)
	return ""
}

func save(buf []byte) (string, error) {
	f, err := ioutil.TempFile("", "artifactory-")
	if err != nil {
		return "", err
	}
	// ioutil.WriteFile accepts no file, only filename
	if _, err := f.Write(buf); err != nil {
		return "", err
	}
	if err := f.Close(); err != nil {
		return "", err
	}
	return f.Name(), nil
}
