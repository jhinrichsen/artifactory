package artifactory

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"path"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"

	"github.com/mitchellh/go-homedir"
)

// Uses mitchellh's homedir(), so will respect environment variable HOME
func userHome() (string, error) {
	return homedir.Dir()
}

func jfrogCliConfigExists() (bool, error) {
	var mu bool
	filename, err := jfrogCliConfigFilename()
	if err != nil {
		return mu, err
	}
	_, err = os.Stat(filename)
	if err == nil {
		return true, err
	}
	if os.IsNotExist(err) {
		return false, err
	}
	return mu, err
}

func jfrogCliConfigFilename() (string, error) {
	h, err := userHome()
	if err != nil {
		return "", err
	}
	return filepath.Join(h, ".jfrog/jfrog-cli.conf"), nil
}

// CliConfiguration represents Version 1 of the JFrog CLI configuration file
// usually found in `${HOME}/.jfrog/jfrog-client.conf`.
type CliConfiguration struct {
	Artifactory []Configuration `json:"artifactory"`
	Version     string          `json:"version"`
}

// HomeCliConfiguration tries to determine the user's HOME directory, and tries
// to read and parse an existing JFrog CLI configuration file.
func HomeCliConfiguration() (CliConfiguration, error) {
	// determine local filename
	var cfg CliConfiguration
	filename, err := jfrogCliConfigFilename()
	if err != nil {
		return cfg, err
	}

	// read file
	buf, err := ioutil.ReadFile(filename)
	if err != nil {
		return cfg, err
	}

	// parse JSON
	if err := json.Unmarshal(buf, &cfg); err != nil {
		return cfg, fmt.Errorf("cannot parse %s: %w", filename, err)
	}
	return cfg, nil
}

// ArtifactoryDefault returns the default Artifactory entry from a CLI
// configuration.
func (cfg CliConfiguration) ArtifactoryDefault() (*Configuration,
	error) {
	for _, a := range cfg.Artifactory {
		if a.IsDefault {
			return &a, nil
		}
	}

	// no artifactory entry, or no default entry
	return nil, fmt.Errorf(
		"no working JFrog CLI setup, maybe run `jfrog rt config`?")
}

// Configuration represents one `artifactory` entry from CLI
// configuration file.
type Configuration struct {
	IsDefault bool   `json:"isDefault"`
	ServerID  string `json:"serverId"`
	URL       string `json:"url"`
	Username  string `json:"user"`
	Password  string `json:"password"`
}

func (a Configuration) serviceURL(uri string) (string, error) {
	u, err := url.Parse(a.URL)
	if err != nil {
		return a.URL, err
	}
	u.Path = path.Join(u.Path, uri)
	return u.String(), nil
}

// SystemPingURL returns REST service URL.
func (a Configuration) SystemPingURL() (string, error) {
	return a.serviceURL("api/system/ping")
}

// SearchArtifactURL returns REST service URL.
func (a Configuration) SearchArtifactURL() (string, error) {
	return a.serviceURL("api/search/artifact")
}

// SystemPing executes remote REST.
func (a Configuration) SystemPing() error {
	u, err := a.SystemPingURL()
	if err != nil {
		return err
	}
	log.Infof("requesting %q\n", u)
	res, err := http.Get(u)
	if err != nil {
		return fmt.Errorf("cannot ping %q: %w", u, err)
	}
	if res.StatusCode != 200 {
		return fmt.Errorf("ping returns status %s", res.Status)
	}
	return nil
}

// Content retrieves a remote artifact via REST.
// URI is the complete path as retrieved from a search via `downloadUri`,
// because artifacts are reachable w/o any REST API. In additon, the context
// root can not always be derived from a JFrog CLI setting.
func (a Configuration) Content(downloadURL string) ([]byte, error) {
	req, err := http.NewRequest(http.MethodGet, downloadURL, nil)
	if err != nil {
		return nil, fmt.Errorf("error constructing request: %w", err)
	}

	log.Infof("requesting (excluding basic auth info) %+v\n", req)

	// basic authentication
	req.SetBasicAuth(a.Username, a.Password)
	clnt := http.DefaultClient
	res, err := clnt.Do(req)
	if err != nil {
		return nil, fmt.Errorf("error fetching artifact: %w", err)
	}
	if res.StatusCode != 200 {
		return nil, fmt.Errorf("want HTTP status code 200 but got %d", res.StatusCode)
	}
	defer res.Body.Close()
	buf, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return nil, fmt.Errorf("error reading HTTP body: %w", err)
	}
	return buf, nil
}

// ArtifactSearchResults represents
// `application/vnd.org.jfrog.artifactory.search.ArtifactSearchResult+json`
type ArtifactSearchResults struct {
	Results []ArtifactSearchResult `json:"results"`
}

// Between filters all search results with any timestamp in the interval
// ]t1..t2[, i.e. t1 < t < t2.
func (a ArtifactSearchResults) Between(t1, t2 time.Time) []ArtifactSearchResult {
	var asrs []ArtifactSearchResult
	for _, asr := range a.Results {
		t := asr.LatestTimestamp()
		if t.After(t1) && t.Before(t2) {
			asrs = append(asrs, asr)
		}
	}
	return asrs
}

// Since filters all search results with any timestamp in the interval
// ]t1..Now[, i.e. t1 < t < Now where Now is the point in time when this
// function is called
func (a ArtifactSearchResults) Since(t1 time.Time) []ArtifactSearchResult {
	return a.Between(t1, time.Now())
}

// Until filters all search results with any timestamp in the interval
// ]0..t2[, i.e. 0 < t < t2 where `0` is the beginning of time.
func (a ArtifactSearchResults) Until(t2 time.Time) []ArtifactSearchResult {
	return a.Between(time.Time{}, t2)
}

// ArtifactSearchResult represents embedded part of
// `application/vnd.org.jfrog.artifactory.search.ArtifactSearchResult+json`
type ArtifactSearchResult struct {
	Created      time.Time                    `json:"created"`
	CreatedBy    string                       `json:"createdBy"`
	DownloadURI  string                       `json:"downloadUri"`
	LastModified time.Time                    `json:"lastModified"`
	LastUpdated  time.Time                    `json:"lastUpdated"`
	MimeType     string                       `json:"mimeType"`
	ModifiedBy   string                       `json:"modifiedBy"`
	Path         string                       `json:"path"`
	Repo         string                       `json:"repo"`
	Checksums    ArtifactSearchResultChecksum `json:"checksums"`
	Size         string                       `json:"size"`
	URI          string                       `json:"uri"`
}

// LatestTimestamp will return the latest timestamp of Created, LastModified and
// LastUpdated. Artifactory does not handle these three timestamps as expected:
// https://www.jfrog.com/jira/browse/RTFACT-17375
// https://www.jfrog.com/jira/browse/RTFACT-19275
func (a ArtifactSearchResult) LatestTimestamp() time.Time {
	latest := a.Created
	if a.LastModified.After(latest) {
		latest = a.LastModified
	}
	if a.LastUpdated.After(latest) {
		latest = a.LastUpdated
	}
	return latest
}

// ArtifactSearchResultChecksum represents embedded part of
// `application/vnd.org.jfrog.artifactory.search.ArtifactSearchResult+json`.
// Golang's JSON module does not support > to reach embedded structs as XML
// does.
type ArtifactSearchResultChecksum struct {
	Sha256 string `json:"sha256"`
}

// SearchParameter contains all required and optional fields for the REST
// service `/search/artifact`.
type SearchParameter struct {
	Repository        string // optional
	Name              string
	IncludeInfo       bool // optional
	IncludeProperties bool // optional
}

// SearchArtifact searches an optional repository for matching artifacts
func (a Configuration) SearchArtifact(sp SearchParameter) (ArtifactSearchResults, error) {
	var asrs ArtifactSearchResults
	s, err := a.SearchArtifactURL()
	if err != nil {
		return asrs, err
	}
	u, err := url.Parse(s)
	if err != nil {
		return asrs, fmt.Errorf("cannot parse URL %q: %w", s, err)
	}

	// request parameter
	q := u.Query()
	if len(sp.Repository) > 0 {
		q.Set("repos", sp.Repository)
	}
	q.Set("name", sp.Name)
	u.RawQuery = q.Encode()

	req, err := http.NewRequest(http.MethodGet, u.String(), nil)
	if err != nil {
		return asrs, fmt.Errorf("error constructing request: %w", err)
	}

	// header
	if sp.IncludeInfo || sp.IncludeProperties {
		var s string
		if sp.IncludeInfo && sp.IncludeProperties {
			s = "info, properties"
		} else if sp.IncludeInfo {
			s = "info"
		} else {
			s = "properties"
		}
		req.Header.Add("X-Result-Detail", s)
	}

	log.Infof("requesting (excluding basic auth info) %+v\n", req)

	// basic authentication
	req.SetBasicAuth(a.Username, a.Password)
	clnt := http.DefaultClient
	res, err := clnt.Do(req)
	if err != nil {
		return asrs, fmt.Errorf("error searching artifact: %w", err)
	}
	if res.StatusCode != 200 {
		return asrs, fmt.Errorf("want HTTP status code 200 but got %d", res.StatusCode)
	}
	defer res.Body.Close()
	buf, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return asrs, fmt.Errorf("error reading HTTP body: %w", err)
	}
	log.Debugf("response body: %+v\n", string(buf))
	if err := json.Unmarshal(buf, &asrs); err != nil {
		return asrs, fmt.Errorf("error parsing %q", buf)
	}
	return asrs, err
}
