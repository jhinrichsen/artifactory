use dirs;

use std::fs::File;
use std::path::PathBuf;

use serde::{Deserialize, Serialize};

/// ---
///{
///  "artifactory": [
///    {
///      "url": "https://.../artifactory/",
///      "user": "<your user ID>",
///      "password": "<your password>",
///      "serverId": "<artifactory ID>",
///      "isDefault": true
///    }
///  ],
///  "Version": "1"
///}
/// ---
#[derive(Serialize, Deserialize)]
struct CliConfiguration {
    artifactory: Vec<ArtifactoryConfiguration>,
    // For whatever reason, Version is capital case
    #[serde(rename(deserialize = "Version"))]
    version: String,
}

#[derive(Clone, Debug, Serialize, Deserialize)]
#[serde(rename_all = "camelCase")]
struct ArtifactoryConfiguration {
    is_default: bool,
    server_id: String,
    url: String,
    user: String,
    password: String,
}

#[derive(Serialize, Deserialize)]
// Rust does not allow nested anonymous structs
struct ArtifactSearchResult {
    results: Vec<ArtifactSearchResultEntry>,
}

#[derive(Serialize, Deserialize)]
struct ArtifactSearchResultEntry {
    uri: String,
}

impl ArtifactoryConfiguration {
    fn artifact(
        name: &str,
        repos: Option<&[&str]>,
        result_detail_info: bool,
        result_detail_properties: bool,
    ) -> Result<ArtifactSearchResult, core::fmt::Error> {
        Ok(ArtifactSearchResult {
            results: Vec::new(),
        })
    }
}

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let cfg = cli_configuration()?;
    let instance = default_artifactory_configuration(&cfg.artifactory)
        .expect("missing default entry in JFROG cli configuration");
    println!("querying default artifactory instance {:?}", instance);
    Ok(())
}

/// Default directory for required JFrog configuration
fn jfrog_home() -> PathBuf {
    let mut p = match dirs::home_dir() {
        Some(p) => p,
        None => panic!("cannot determine home directory"),
    };
    p.push(".jfrog");
    p
}

/// Default path for required JFrog configuration
/// The extension is `.conf` although it is YAML
fn jfrog_cfg_filename() -> PathBuf {
    let mut p = jfrog_home();
    p.push("jfrog-cli");
    p.set_extension("conf");
    p
}

fn cli_configuration() -> Result<CliConfiguration, std::io::Error> {
    let f = File::open(jfrog_cfg_filename())?;
    let p: CliConfiguration = serde_json::from_reader(f)?;
    Ok(p)
}

fn default_artifactory_configuration<'a>(
    acs: &'a [ArtifactoryConfiguration],
) -> Option<&'a ArtifactoryConfiguration> {
    acs.iter().find(|c| c.is_default)
}

#[cfg(test)]
mod tests {

    use super::ArtifactSearchResult;
    use super::CliConfiguration;

    // use futures::future::Future;
    use hyper::rt::{self, Future, Stream};
    use hyper::Client;

    #[test]
    fn parse_sample_cli_configuration() {
        let s = r#"
            {
              "artifactory": [
                {
                  "url": "https://.../artifactory/",
                  "user": "<your user ID>",
                  "password": "<your password>",
                  "serverId": "ID7",
                  "isDefault": true
                }
              ],
              "Version": "1"
            }
            "#;
        let p: CliConfiguration = serde_json::from_str(s).unwrap();
        assert_eq!(1, p.artifactory.len());
        assert_eq!("1", p.version);
        let a = &p.artifactory[0];
        assert_eq!("https://.../artifactory/", a.url);
        assert_eq!(true, a.is_default);
        assert_eq!("ID7", a.server_id);
    }

    #[test]
    fn test_search_result() {
        let sample_result = r#"
	  {
            "results": [
              {
                "uri": "http://localhost:8081/artifactory/api/storage/libs-release-local/org/acme/lib/ver/lib-ver.pom"
              },{
                "uri": "http://localhost:8081/artifactory/api/storage/libs-release-local/org/acme/lib/ver2/lib-ver2.pom"
              }
              ]
              }
        "#;
        let sr: ArtifactSearchResult = serde_json::from_str(sample_result).unwrap();
        assert_eq!(2, sr.results.len());
        let u = "http://localhost:8081/artifactory/api/storage/libs-release-local/org/acme/lib/ver2/lib-ver2.pom";
        assert_eq!(u, sr.results[1].uri)
    }

    // #[cfg_attr(not(feature = "expensive_tests"), ignore)]
    // Validate local JFrog CLI setup and connection to default remote instance
    #[test]
    fn online_ping() {
        rt::run(rt::lazy(|| {
            // This is main future that the runtime will execute.
            //
            // The `lazy` is because we don't want any of this executing *right now*,
            // but rather once the runtime has started up all its resources.
            //
            // This is where we will setup our HTTP client requests.
            let client = Client::new();

            let uri = "https://repo.jfrog.com/artifactory/api/system/ping"
                .parse()
                .unwrap();

            client
                .get(uri)
                .map(|res| {
                    // println!("Response: {}", res.status());
                    println!("Response: {:?}", res);
                })
                .map_err(|err| {
                    println!("Error: {}", err);
                })
        }));
    }
}
