module gitlab.com/jhinrichsen/artifactory

go 1.13

require (
	github.com/mitchellh/go-homedir v1.1.0
	github.com/sirupsen/logrus v1.4.2
)
