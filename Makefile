.PHONY: tidy
tidy:
	test -z $(gofmt -l .)
	golint -set_exit_status
	go vet
	go mod tidy
